EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "LTC2208"
Date "2020-01-31"
Rev "1"
Comp "Harmon Instruments"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L misc:testpoint_1mm TP?
U 1 1 5E8AFB1E
P 5550 3000
AR Path="/5BE20C19/5E8AFB1E" Ref="TP?"  Part="1" 
AR Path="/5E8AFB1E" Ref="TP2"  Part="1" 
F 0 "TP2" H 5700 2800 50  0000 L CNN
F 1 "testpoint_1mm" H 5750 2800 50  0001 L CNN
F 2 "kicad_pcb:TP_0.5mm" H 5750 2900 50  0001 L CNN
F 3 "" V 5800 3000 50  0001 C CNN
	1    5550 3000
	1    0    0    -1  
$EndComp
$Comp
L connector:HF9MW J1
U 1 1 5EC60C6F
P 6750 2800
F 0 "J1" H 6900 2873 50  0000 C CNN
F 1 "HF9MW" H 6750 2850 50  0001 L CNN
F 2 "kicad_pcb:fpc_0.3_45_socket" H 6750 2850 50  0001 L CNN
F 3 "$PARTS/HF9MW/ENG_CD_2328724_3.pdf" V 7000 2800 50  0001 C CNN
	1    6750 2800
	-1   0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 631BFAE8
P 7400 5500
F 0 "FB1" V 7163 5500 50  0000 C CNN
F 1 "fb0402" V 7254 5500 50  0000 C CNN
F 2 "kicad_pcb:L_0402_1005Metric" V 7330 5500 50  0001 C CNN
F 3 "~" H 7400 5500 50  0001 C CNN
	1    7400 5500
	0    -1   1    0   
$EndComp
Wire Wire Line
	6900 3000 6900 3100
Connection ~ 6900 3100
Wire Wire Line
	6900 3100 6900 3200
Connection ~ 6900 3200
Wire Wire Line
	6900 3200 6900 3300
Connection ~ 6900 3300
Wire Wire Line
	6900 3300 6900 3400
Connection ~ 6900 3400
Wire Wire Line
	6900 3400 6900 3500
Connection ~ 6900 3500
Wire Wire Line
	6900 3500 6900 3600
Connection ~ 6900 3600
Wire Wire Line
	6900 3600 6900 3700
Connection ~ 6900 3700
Wire Wire Line
	6900 3700 6900 3800
Connection ~ 6900 3800
Wire Wire Line
	6900 3800 6900 3900
Connection ~ 6900 3900
Wire Wire Line
	6900 3900 6900 4000
Connection ~ 6900 4000
Wire Wire Line
	6900 4000 6900 4100
Connection ~ 6900 4100
Wire Wire Line
	6900 4100 6900 4200
Connection ~ 6900 4200
Wire Wire Line
	6900 4200 6900 4300
Connection ~ 6900 4300
Wire Wire Line
	6900 4300 6900 4400
Connection ~ 6900 4400
Wire Wire Line
	6900 4400 6900 4500
Connection ~ 6900 4500
Wire Wire Line
	6900 4500 6900 4600
Connection ~ 6900 4600
Wire Wire Line
	6900 4600 6900 4700
Connection ~ 6900 4700
Wire Wire Line
	6900 4700 6900 4800
Connection ~ 6900 4800
Wire Wire Line
	6900 4800 6900 4900
Connection ~ 6900 4900
Wire Wire Line
	6900 4900 6900 5000
Wire Wire Line
	6900 5000 7000 5000
Wire Wire Line
	7000 5000 7000 5200
Wire Wire Line
	7000 5200 6900 5200
Connection ~ 6900 5000
Wire Wire Line
	7000 5200 7000 5300
Wire Wire Line
	7000 5300 6900 5300
Connection ~ 7000 5200
Wire Wire Line
	7000 5300 7000 5400
Connection ~ 7000 5300
$Comp
L combined:ground #PWR?
U 1 1 65AC1BF1
P 7000 5400
AR Path="/5BE20C19/65AC1BF1" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/65AC1BF1" Ref="#PWR?"  Part="1" 
AR Path="/65AC1BF1" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 7000 5400 50  0001 C CNN
F 1 "ground" H 7000 5330 50  0001 C CNN
F 2 "" H 7000 5400 50  0001 C CNN
F 3 "" H 7000 5400 50  0000 C CNN
	1    7000 5400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6300 2950 6200 2950
Wire Wire Line
	6200 2950 6200 2600
Wire Wire Line
	6950 2600 6950 2900
Wire Wire Line
	6950 2900 6900 2900
Wire Wire Line
	6900 5100 7150 5100
Wire Wire Line
	7150 5100 7150 5500
Wire Wire Line
	7150 5500 6250 5500
Wire Wire Line
	6250 5500 6250 5050
Wire Wire Line
	6250 5050 6300 5050
$Comp
L misc:tooling_hole TH3
U 1 1 5BBD5C94
P 750 7050
F 0 "TH3" H 850 7050 50  0000 L CNN
F 1 "tooling_hole" H 900 7050 50  0001 L CNN
F 2 "kicad_pcb:tooling_hole_2.55" H 900 7150 50  0001 L CNN
F 3 "" V 950 7250 50  0001 C CNN
	1    750  7050
	1    0    0    -1  
$EndComp
$Comp
L misc:tooling_hole TH4
U 1 1 5BBD5C9A
P 750 7200
F 0 "TH4" H 850 7200 50  0000 L CNN
F 1 "tooling_hole" H 900 7200 50  0001 L CNN
F 2 "kicad_pcb:tooling_hole_2.55" H 900 7300 50  0001 L CNN
F 3 "" V 950 7400 50  0001 C CNN
	1    750  7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 2550 9950 2600
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E40CC2E
P 9150 2800
AR Path="/5C2CDB08/5E40CC2E" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E40CC2E" Ref="C?"  Part="1" 
AR Path="/5E40CC2E" Ref="C1"  Part="1" 
F 0 "C1" V 9000 2800 50  0000 C CNN
F 1 "1 uF" V 9300 2800 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 9300 2700 50  0001 L CNN
F 3 "" H 9150 2800 50  0000 C CNN
	1    9150 2800
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5E40CC34
P 3950 2000
AR Path="/5C2CDB08/5E40CC34" Ref="#FLG?"  Part="1" 
AR Path="/5E374CB3/5E40CC34" Ref="#FLG?"  Part="1" 
AR Path="/5E40CC34" Ref="#FLG0102"  Part="1" 
F 0 "#FLG0102" H 3950 2075 50  0001 C CNN
F 1 "PWR_FLAG" V 3950 2300 50  0000 C CNN
F 2 "" H 3950 2000 50  0001 C CNN
F 3 "~" H 3950 2000 50  0001 C CNN
	1    3950 2000
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E40CC3A
P 3950 2000
AR Path="/5C2CDB08/5E40CC3A" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E40CC3A" Ref="#PWR?"  Part="1" 
AR Path="/5E40CC3A" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 3950 2000 50  0001 C CNN
F 1 "ground" H 3950 1930 50  0001 C CNN
F 2 "" H 3950 2000 50  0001 C CNN
F 3 "" H 3950 2000 50  0000 C CNN
	1    3950 2000
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E40CC40
P 9950 3050
AR Path="/5C2CDB08/5E40CC40" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E40CC40" Ref="#PWR?"  Part="1" 
AR Path="/5E40CC40" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 9950 3050 50  0001 C CNN
F 1 "ground" H 9950 2980 50  0001 C CNN
F 2 "" H 9950 3050 50  0001 C CNN
F 3 "" H 9950 3050 50  0000 C CNN
	1    9950 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 3000 9950 3050
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E40CC47
P 9550 2800
AR Path="/5C2CDB08/5E40CC47" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E40CC47" Ref="C?"  Part="1" 
AR Path="/5E40CC47" Ref="C2"  Part="1" 
F 0 "C2" V 9400 2800 50  0000 C CNN
F 1 "1 uF" V 9700 2800 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 9700 2700 50  0001 L CNN
F 3 "" H 9550 2800 50  0000 C CNN
	1    9550 2800
	1    0    0    -1  
$EndComp
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E40CC4D
P 9950 2800
AR Path="/5C2CDB08/5E40CC4D" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E40CC4D" Ref="C?"  Part="1" 
AR Path="/5E40CC4D" Ref="C3"  Part="1" 
F 0 "C3" V 9800 2800 50  0000 C CNN
F 1 "1 uF" V 10100 2800 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 10100 2700 50  0001 L CNN
F 3 "" H 9950 2800 50  0000 C CNN
	1    9950 2800
	1    0    0    -1  
$EndComp
Connection ~ 9550 2600
Wire Wire Line
	9550 2600 9950 2600
Connection ~ 9550 3000
Wire Wire Line
	9550 3000 9950 3000
Connection ~ 9950 2600
Connection ~ 9950 3000
Wire Wire Line
	9150 2600 9550 2600
Wire Wire Line
	9150 3000 9550 3000
$Comp
L power:+3V8 #PWR0105
U 1 1 5E40CC5B
P 8100 2550
AR Path="/5E40CC5B" Ref="#PWR0105"  Part="1" 
AR Path="/5C2CDB08/5E40CC5B" Ref="#PWR?"  Part="1" 
F 0 "#PWR0105" H 8100 2400 50  0001 C CNN
F 1 "+3V8" H 8115 2723 50  0000 C CNN
F 2 "" H 8100 2550 50  0001 C CNN
F 3 "" H 8100 2550 50  0001 C CNN
	1    8100 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2550 8100 2600
Wire Wire Line
	8150 2600 8100 2600
$Comp
L ic:DR2TQ U1
U 1 1 5E40CC63
P 8250 2500
AR Path="/5E40CC63" Ref="U1"  Part="1" 
AR Path="/5C2CDB08/5E40CC63" Ref="U?"  Part="1" 
F 0 "U1" H 8550 2665 50  0000 C CNN
F 1 "DR2TQ" H 8550 2574 50  0000 C CNN
F 2 "kicad_pcb:DFN-6-1EP_2x2mm_P0.65mm_EP1x1.6mm" H 8550 2550 50  0001 L CNN
F 3 "${PARTS}/TTGT9/lp5912.pdf" V 8500 2500 50  0001 C CNN
	1    8250 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 2600 8950 2600
Connection ~ 9150 2600
Wire Wire Line
	8450 3000 8550 3000
Connection ~ 8550 3000
Wire Wire Line
	8550 3000 8650 3000
Connection ~ 8650 3000
Wire Wire Line
	8650 3000 8750 3000
Wire Wire Line
	8750 3050 8750 3000
Connection ~ 8750 3000
$Comp
L combined:ground #PWR?
U 1 1 5E40CC72
P 8750 3050
AR Path="/5C2CDB08/5E40CC72" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E40CC72" Ref="#PWR?"  Part="1" 
AR Path="/5E40CC72" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 8750 3050 50  0001 C CNN
F 1 "ground" H 8750 2980 50  0001 C CNN
F 2 "" H 8750 3050 50  0001 C CNN
F 3 "" H 8750 3050 50  0000 C CNN
	1    8750 3050
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5E40CC78
P 9950 2550
AR Path="/5C2CDB08/5E40CC78" Ref="#PWR?"  Part="1" 
AR Path="/5E40CC78" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 9950 2400 50  0001 C CNN
F 1 "+3V3" H 9965 2723 50  0000 C CNN
F 2 "" H 9950 2550 50  0001 C CNN
F 3 "" H 9950 2550 50  0001 C CNN
	1    9950 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+1V8 #PWR?
U 1 1 5E40CC86
P 8200 5450
AR Path="/5C2CDB08/5E40CC86" Ref="#PWR?"  Part="1" 
AR Path="/5E40CC86" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 8200 5300 50  0001 C CNN
F 1 "+1V8" H 8215 5623 50  0000 C CNN
F 2 "" H 8200 5450 50  0001 C CNN
F 3 "" H 8200 5450 50  0001 C CNN
	1    8200 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2600 8100 2800
Wire Wire Line
	8100 2800 8150 2800
Connection ~ 8100 2600
$Comp
L ic:LTC2208 U2
U 1 1 5E4BC350
P 3950 2750
F 0 "U2" H 4000 2900 50  0000 C CNN
F 1 "LTC2208" H 4100 2800 50  0000 C CNN
F 2 "kicad_pcb:QFN-64-1EP_9x9mm_P0.5mm_EP6.2x6.2mm" H 4150 2900 50  0001 L CNN
F 3 "" V 4200 2750 50  0001 C CNN
	1    3950 2750
	1    0    0    -1  
$EndComp
$Comp
L misc:transformer_CT T1
U 1 1 5E4CCB8E
P 2100 3950
F 0 "T1" H 2100 4421 50  0000 C CNN
F 1 "TC2-72T+" H 2100 4330 50  0000 C CNN
F 2 "kicad_pcb:Macom_SM-22" H 2100 4239 50  0001 C CNN
F 3 "" H 2100 3950 50  0001 C CNN
	1    2100 3950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2300 4150 2750 4150
Wire Wire Line
	3850 4150 3850 4000
Wire Wire Line
	3850 3900 3850 3750
Wire Wire Line
	2300 3950 2400 3950
Wire Wire Line
	2450 3950 2450 3650
$Comp
L combined:ground #PWR?
U 1 1 5E4DA33C
P 4950 5350
AR Path="/5BE20C19/5E4DA33C" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E4DA33C" Ref="#PWR?"  Part="1" 
AR Path="/5E4DA33C" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 4950 5350 50  0001 C CNN
F 1 "ground" H 4950 5280 50  0001 C CNN
F 2 "" H 4950 5350 50  0001 C CNN
F 3 "" H 4950 5350 50  0000 C CNN
	1    4950 5350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4950 5350 4950 5300
Wire Wire Line
	4050 5300 4150 5300
Connection ~ 4950 5300
Connection ~ 4150 5300
Wire Wire Line
	4150 5300 4250 5300
Connection ~ 4250 5300
Wire Wire Line
	4250 5300 4350 5300
Connection ~ 4350 5300
Wire Wire Line
	4350 5300 4450 5300
Connection ~ 4450 5300
Wire Wire Line
	4450 5300 4550 5300
Connection ~ 4550 5300
Wire Wire Line
	4550 5300 4650 5300
Connection ~ 4650 5300
Wire Wire Line
	4650 5300 4750 5300
Connection ~ 4750 5300
Wire Wire Line
	4750 5300 4850 5300
Connection ~ 4850 5300
Wire Wire Line
	4850 5300 4950 5300
Connection ~ 4450 2650
Wire Wire Line
	4450 2650 4350 2650
Connection ~ 4550 2650
Wire Wire Line
	4550 2650 4450 2650
Connection ~ 4650 2650
Wire Wire Line
	4650 2650 4550 2650
Wire Wire Line
	4750 2650 4650 2650
Wire Wire Line
	4850 2650 4950 2650
$Comp
L power:+1V8 #PWR0107
U 1 1 5E4EE147
P 4950 2650
F 0 "#PWR0107" H 4950 2500 50  0001 C CNN
F 1 "+1V8" H 4965 2823 50  0000 C CNN
F 2 "" H 4950 2650 50  0001 C CNN
F 3 "" H 4950 2650 50  0001 C CNN
	1    4950 2650
	-1   0    0    -1  
$EndComp
Connection ~ 4950 2650
$Comp
L power:+3V3 #PWR?
U 1 1 5E4EE89A
P 4350 2650
AR Path="/5C2CDB08/5E4EE89A" Ref="#PWR?"  Part="1" 
AR Path="/5E4EE89A" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 4350 2500 50  0001 C CNN
F 1 "+3V3" H 4365 2823 50  0000 C CNN
F 2 "" H 4350 2650 50  0001 C CNN
F 3 "" H 4350 2650 50  0001 C CNN
	1    4350 2650
	1    0    0    -1  
$EndComp
Connection ~ 4350 2650
$Comp
L misc:BWQDQ S1
U 1 1 5E4F7D90
P 2200 6800
F 0 "S1" H 2830 7043 50  0000 L CNN
F 1 "BWQDQ" H 2400 6800 50  0001 L CNN
F 2 "kicad_pcb:Laird_Technologies_BMI-S-209-F_29.36x18.50mm" H 2400 6900 50  0001 L CNN
F 3 "$PARTS/BWQDQ/drawing.pdf" V 2450 6750 50  0001 C CNN
	1    2200 6800
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E4F863A
P 2200 6900
AR Path="/5CDB68FA/5E4F863A" Ref="#PWR?"  Part="1" 
AR Path="/5DFE1F9E/5E4F863A" Ref="#PWR?"  Part="1" 
AR Path="/5E4F863A" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 2200 6900 50  0001 C CNN
F 1 "ground" H 2200 6830 50  0001 C CNN
F 2 "" H 2200 6900 50  0001 C CNN
F 3 "" H 2200 6900 50  0000 C CNN
	1    2200 6900
	-1   0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E506C05
P 1900 3750
AR Path="/5CDB68FA/5E506C05" Ref="#PWR?"  Part="1" 
AR Path="/5DFE1F9E/5E506C05" Ref="#PWR?"  Part="1" 
AR Path="/5E506C05" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 1900 3750 50  0001 C CNN
F 1 "ground" H 1900 3680 50  0001 C CNN
F 2 "" H 1900 3750 50  0001 C CNN
F 3 "" H 1900 3750 50  0000 C CNN
	1    1900 3750
	0    1    -1   0   
$EndComp
$Comp
L capacitors:capacitor_0603 C9
U 1 1 5E509B8E
P 3550 3350
F 0 "C9" H 3685 3396 50  0000 L CNN
F 1 "2.2 uF" H 3685 3305 50  0000 L CNN
F 2 "kicad_pcb:C_0603_1608Metric" H 3700 3250 50  0001 L CNN
F 3 "" H 3550 3350 50  0000 C CNN
	1    3550 3350
	1    0    0    -1  
$EndComp
Connection ~ 3550 3150
Wire Wire Line
	3550 3150 3850 3150
$Comp
L combined:ground #PWR?
U 1 1 5E50FE3F
P 3550 3550
AR Path="/5BE20C19/5E50FE3F" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E50FE3F" Ref="#PWR?"  Part="1" 
AR Path="/5E50FE3F" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 3550 3550 50  0001 C CNN
F 1 "ground" H 3550 3480 50  0001 C CNN
F 2 "" H 3550 3550 50  0001 C CNN
F 3 "" H 3550 3550 50  0000 C CNN
	1    3550 3550
	-1   0    0    -1  
$EndComp
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E5190EE
P 2400 4400
AR Path="/5C2CDB08/5E5190EE" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E5190EE" Ref="C?"  Part="1" 
AR Path="/5E5190EE" Ref="C4"  Part="1" 
F 0 "C4" V 2250 4400 50  0000 C CNN
F 1 "1 uF" V 2550 4400 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 2550 4300 50  0001 L CNN
F 3 "" H 2400 4400 50  0000 C CNN
	1    2400 4400
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E519BAE
P 2400 4600
AR Path="/5BE20C19/5E519BAE" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E519BAE" Ref="#PWR?"  Part="1" 
AR Path="/5E519BAE" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 2400 4600 50  0001 C CNN
F 1 "ground" H 2400 4530 50  0001 C CNN
F 2 "" H 2400 4600 50  0001 C CNN
F 3 "" H 2400 4600 50  0000 C CNN
	1    2400 4600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2400 4200 2400 3950
Connection ~ 2400 3950
Wire Wire Line
	2400 3950 2450 3950
$Comp
L resistor:resistor_0402 R?
U 1 1 5E52707E
P 2450 3400
AR Path="/5C2CDB08/5E52707E" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E52707E" Ref="R?"  Part="1" 
AR Path="/5E52707E" Ref="R1"  Part="1" 
F 0 "R1" V 2350 3400 50  0000 C CNN
F 1 "5" V 2450 3400 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 2350 3200 50  0001 C CNN
F 3 "" H 2450 3400 50  0000 C CNN
	1    2450 3400
	1    0    0    -1  
$EndComp
$Comp
L resistor:resistor_0402 R?
U 1 1 5E52808D
P 2900 3700
AR Path="/5C2CDB08/5E52808D" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E52808D" Ref="R?"  Part="1" 
AR Path="/5E52808D" Ref="R4"  Part="1" 
F 0 "R4" V 2800 3700 50  0000 C CNN
F 1 "49.9" V 2900 3700 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 2800 3500 50  0001 C CNN
F 3 "" H 2900 3700 50  0000 C CNN
	1    2900 3700
	1    0    0    1   
$EndComp
$Comp
L resistor:resistor_0402 R?
U 1 1 5E528604
P 2900 4200
AR Path="/5C2CDB08/5E528604" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E528604" Ref="R?"  Part="1" 
AR Path="/5E528604" Ref="R5"  Part="1" 
F 0 "R5" V 2800 4200 50  0000 C CNN
F 1 "49.9" V 2900 4200 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 2800 4000 50  0001 C CNN
F 3 "" H 2900 4200 50  0000 C CNN
	1    2900 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3950 2900 3950
Connection ~ 2450 3950
Connection ~ 2900 3950
Wire Wire Line
	2900 4450 3050 4450
Wire Wire Line
	3050 4450 3050 4150
Wire Wire Line
	3050 4150 3150 4150
Wire Wire Line
	3050 3750 3050 3450
Wire Wire Line
	3050 3450 2900 3450
Wire Wire Line
	2900 3450 2750 3450
Wire Wire Line
	2750 3450 2750 3750
Connection ~ 2900 3450
Wire Wire Line
	2750 3750 2300 3750
Wire Wire Line
	2900 4450 2750 4450
Wire Wire Line
	2750 4450 2750 4150
Connection ~ 2900 4450
Wire Wire Line
	2450 3150 3550 3150
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E565EDC
P 3150 3950
AR Path="/5C2CDB08/5E565EDC" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E565EDC" Ref="C?"  Part="1" 
AR Path="/5E565EDC" Ref="C5"  Part="1" 
F 0 "C5" V 3000 3950 50  0000 C CNN
F 1 "8.2 pF" V 3300 3950 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 3300 3850 50  0001 L CNN
F 3 "" H 3150 3950 50  0000 C CNN
	1    3150 3950
	1    0    0    -1  
$EndComp
Connection ~ 3150 3750
Wire Wire Line
	3150 3750 3050 3750
Connection ~ 3150 4150
Wire Wire Line
	3150 4150 3250 4150
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E5667C1
P 3250 4350
AR Path="/5C2CDB08/5E5667C1" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E5667C1" Ref="C?"  Part="1" 
AR Path="/5E5667C1" Ref="C8"  Part="1" 
F 0 "C8" V 3100 4350 50  0000 C CNN
F 1 "8.2 pF" V 3400 4350 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 3400 4250 50  0001 L CNN
F 3 "" H 3250 4350 50  0000 C CNN
	1    3250 4350
	1    0    0    -1  
$EndComp
Connection ~ 3250 4150
Wire Wire Line
	3250 4150 3300 4150
$Comp
L combined:ground #PWR?
U 1 1 5E566BCB
P 3250 4550
AR Path="/5BE20C19/5E566BCB" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E566BCB" Ref="#PWR?"  Part="1" 
AR Path="/5E566BCB" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 3250 4550 50  0001 C CNN
F 1 "ground" H 3250 4480 50  0001 C CNN
F 2 "" H 3250 4550 50  0001 C CNN
F 3 "" H 3250 4550 50  0000 C CNN
	1    3250 4550
	-1   0    0    -1  
$EndComp
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E566FC4
P 3250 3550
AR Path="/5C2CDB08/5E566FC4" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E566FC4" Ref="C?"  Part="1" 
AR Path="/5E566FC4" Ref="C7"  Part="1" 
F 0 "C7" V 3100 3550 50  0000 C CNN
F 1 "8.2 pF" V 3400 3550 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 3400 3450 50  0001 L CNN
F 3 "" H 3250 3550 50  0000 C CNN
	1    3250 3550
	1    0    0    -1  
$EndComp
Connection ~ 3250 3750
Wire Wire Line
	3250 3750 3150 3750
$Comp
L combined:ground #PWR?
U 1 1 5E56748F
P 3250 3350
AR Path="/5BE20C19/5E56748F" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E56748F" Ref="#PWR?"  Part="1" 
AR Path="/5E56748F" Ref="#PWR0120"  Part="1" 
F 0 "#PWR0120" H 3250 3350 50  0001 C CNN
F 1 "ground" H 3250 3280 50  0001 C CNN
F 2 "" H 3250 3350 50  0001 C CNN
F 3 "" H 3250 3350 50  0000 C CNN
	1    3250 3350
	1    0    0    1   
$EndComp
Wire Wire Line
	3850 3750 3800 3750
$Comp
L resistor:resistor_0402 R?
U 1 1 5E576D6D
P 3550 3750
AR Path="/5C2CDB08/5E576D6D" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E576D6D" Ref="R?"  Part="1" 
AR Path="/5E576D6D" Ref="R6"  Part="1" 
F 0 "R6" V 3450 3750 50  0000 C CNN
F 1 "5" V 3550 3750 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 3450 3550 50  0001 C CNN
F 3 "" H 3550 3750 50  0000 C CNN
	1    3550 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 3750 3250 3750
$Comp
L resistor:resistor_0402 R?
U 1 1 5E5773AD
P 3550 4150
AR Path="/5C2CDB08/5E5773AD" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E5773AD" Ref="R?"  Part="1" 
AR Path="/5E5773AD" Ref="R7"  Part="1" 
F 0 "R7" V 3450 4150 50  0000 C CNN
F 1 "5" V 3550 4150 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 3450 3950 50  0001 C CNN
F 3 "" H 3550 4150 50  0000 C CNN
	1    3550 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	3800 4150 3850 4150
$Comp
L connector:9432D J2
U 1 1 5E58457C
P 1750 4150
F 0 "J2" H 1704 4388 50  0000 C CNN
F 1 "9432D" H 1704 4297 50  0000 C CNN
F 2 "kicad_pcb:SMA_TH" H 1750 4300 50  0001 C CNN
F 3 "" H 1750 4150 50  0001 C CNN
	1    1750 4150
	-1   0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E5854BB
P 1750 4300
AR Path="/5BE20C19/5E5854BB" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E5854BB" Ref="#PWR?"  Part="1" 
AR Path="/5E5854BB" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 1750 4300 50  0001 C CNN
F 1 "ground" H 1750 4230 50  0001 C CNN
F 2 "" H 1750 4300 50  0001 C CNN
F 3 "" H 1750 4300 50  0000 C CNN
	1    1750 4300
	-1   0    0    -1  
$EndComp
$Comp
L resistor:resistor_0402 R?
U 1 1 5E59251C
P 3250 5400
AR Path="/5C2CDB08/5E59251C" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E59251C" Ref="R?"  Part="1" 
AR Path="/5E59251C" Ref="R10"  Part="1" 
F 0 "R10" V 3150 5400 50  0000 C CNN
F 1 "100" V 3250 5400 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 3150 5200 50  0001 C CNN
F 3 "" H 3250 5400 50  0000 C CNN
	1    3250 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 4300 3500 4300
Wire Wire Line
	3500 4300 3500 5150
Wire Wire Line
	3500 5150 3350 5150
Wire Wire Line
	3850 4400 3550 4400
Wire Wire Line
	3550 4400 3550 5650
Wire Wire Line
	3550 5650 3350 5650
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E59E4B2
P 2900 5400
AR Path="/5C2CDB08/5E59E4B2" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E59E4B2" Ref="C?"  Part="1" 
AR Path="/5E59E4B2" Ref="C13"  Part="1" 
F 0 "C13" V 2750 5400 50  0000 C CNN
F 1 "33 pF" V 3050 5400 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 3050 5300 50  0001 L CNN
F 3 "" H 2900 5400 50  0000 C CNN
	1    2900 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 5150 2900 5150
Wire Wire Line
	2900 5150 2900 5200
Connection ~ 3250 5150
Wire Wire Line
	3250 5650 2900 5650
Wire Wire Line
	2900 5650 2900 5600
Connection ~ 3250 5650
$Comp
L resistor:resistor_0402 R?
U 1 1 5E5C74FD
P 2250 5150
AR Path="/5C2CDB08/5E5C74FD" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E5C74FD" Ref="R?"  Part="1" 
AR Path="/5E5C74FD" Ref="R8"  Part="1" 
F 0 "R8" V 2150 5150 50  0000 C CNN
F 1 "49.9" V 2250 5150 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 2150 4950 50  0001 C CNN
F 3 "" H 2250 5150 50  0000 C CNN
	1    2250 5150
	1    0    0    1   
$EndComp
$Comp
L resistor:resistor_0402 R?
U 1 1 5E5C7507
P 2250 5650
AR Path="/5C2CDB08/5E5C7507" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E5C7507" Ref="R?"  Part="1" 
AR Path="/5E5C7507" Ref="R9"  Part="1" 
F 0 "R9" V 2150 5650 50  0000 C CNN
F 1 "49.9" V 2250 5650 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 2150 5450 50  0001 C CNN
F 3 "" H 2250 5650 50  0000 C CNN
	1    2250 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4900 2400 4900
Wire Wire Line
	2900 4900 2900 5150
Connection ~ 2900 5150
Wire Wire Line
	2250 5900 2400 5900
Wire Wire Line
	2900 5900 2900 5650
Connection ~ 2900 5650
$Comp
L combined:ground #PWR?
U 1 1 5E5E0421
P 2200 5400
AR Path="/5BE20C19/5E5E0421" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E5E0421" Ref="#PWR?"  Part="1" 
AR Path="/5E5E0421" Ref="#PWR0122"  Part="1" 
F 0 "#PWR0122" H 2200 5400 50  0001 C CNN
F 1 "ground" H 2200 5330 50  0001 C CNN
F 2 "" H 2200 5400 50  0001 C CNN
F 3 "" H 2200 5400 50  0000 C CNN
	1    2200 5400
	0    1    -1   0   
$EndComp
$Comp
L misc:CT6XQ T2
U 1 1 5E5ED7FF
P 1650 5400
F 0 "T2" H 1650 5681 50  0000 C CNN
F 1 "CT6XQ" H 1650 5590 50  0000 C CNN
F 2 "kicad_pcb:Macom_SM-22" H 1650 5250 50  0001 C CNN
F 3 "$PARTS/CT6XQ/MABA-007159-000000-13414.pdf" V 1650 5400 50  0001 C CNN
	1    1650 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 5900 2050 5900
Wire Wire Line
	2050 5900 2050 5500
Connection ~ 2250 5900
Wire Wire Line
	2050 5300 2050 4900
Wire Wire Line
	2050 4900 2250 4900
Connection ~ 2250 4900
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E60AC0D
P 2600 5900
AR Path="/5C2CDB08/5E60AC0D" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E60AC0D" Ref="C?"  Part="1" 
AR Path="/5E60AC0D" Ref="C10"  Part="1" 
F 0 "C10" V 2450 5900 50  0000 C CNN
F 1 "1 uF" V 2750 5900 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 2750 5800 50  0001 L CNN
F 3 "" H 2600 5900 50  0000 C CNN
	1    2600 5900
	0    1    1    0   
$EndComp
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E60B4AB
P 2600 4900
AR Path="/5C2CDB08/5E60B4AB" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E60B4AB" Ref="C?"  Part="1" 
AR Path="/5E60B4AB" Ref="C11"  Part="1" 
F 0 "C11" V 2450 4900 50  0000 C CNN
F 1 "1 uF" V 2750 4900 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 2750 4800 50  0001 L CNN
F 3 "" H 2600 4900 50  0000 C CNN
	1    2600 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 5500 1050 5500
$Comp
L combined:ground #PWR?
U 1 1 5E61DC9C
P 1200 5350
AR Path="/5BE20C19/5E61DC9C" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E61DC9C" Ref="#PWR?"  Part="1" 
AR Path="/5E61DC9C" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 1200 5350 50  0001 C CNN
F 1 "ground" H 1200 5280 50  0001 C CNN
F 2 "" H 1200 5350 50  0001 C CNN
F 3 "" H 1200 5350 50  0000 C CNN
	1    1200 5350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1250 5300 1200 5300
Wire Wire Line
	1200 5300 1200 5350
$Comp
L connector:9432D J3
U 1 1 5E629F14
P 900 5500
F 0 "J3" H 854 5738 50  0000 C CNN
F 1 "9432D" H 854 5647 50  0000 C CNN
F 2 "kicad_pcb:SMA_TH" H 900 5650 50  0001 C CNN
F 3 "" H 900 5500 50  0001 C CNN
	1    900  5500
	-1   0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E629F1E
P 900 5650
AR Path="/5BE20C19/5E629F1E" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E629F1E" Ref="#PWR?"  Part="1" 
AR Path="/5E629F1E" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 900 5650 50  0001 C CNN
F 1 "ground" H 900 5580 50  0001 C CNN
F 2 "" H 900 5650 50  0001 C CNN
F 3 "" H 900 5650 50  0000 C CNN
	1    900  5650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3850 4800 3850 4900
Wire Wire Line
	3850 5300 4050 5300
Connection ~ 3850 4900
Wire Wire Line
	3850 4900 3850 5300
Connection ~ 4050 5300
$Comp
L power:+3V3 #PWR?
U 1 1 5E65092C
P 3850 4500
AR Path="/5C2CDB08/5E65092C" Ref="#PWR?"  Part="1" 
AR Path="/5E65092C" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 3850 4350 50  0001 C CNN
F 1 "+3V3" H 3865 4673 50  0000 C CNN
F 2 "" H 3850 4500 50  0001 C CNN
F 3 "" H 3850 4500 50  0001 C CNN
	1    3850 4500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3850 4700 3850 4800
Connection ~ 3850 4800
$Comp
L power:+3V3 #PWR?
U 1 1 5E65A534
P 3350 3050
AR Path="/5C2CDB08/5E65A534" Ref="#PWR?"  Part="1" 
AR Path="/5E65A534" Ref="#PWR0126"  Part="1" 
F 0 "#PWR0126" H 3350 2900 50  0001 C CNN
F 1 "+3V3" H 3365 3223 50  0000 C CNN
F 2 "" H 3350 3050 50  0001 C CNN
F 3 "" H 3350 3050 50  0001 C CNN
	1    3350 3050
	0    -1   -1   0   
$EndComp
$Comp
L resistor:resistor_0402 R?
U 1 1 5E65ADD3
P 3600 3050
AR Path="/5C2CDB08/5E65ADD3" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E65ADD3" Ref="R?"  Part="1" 
AR Path="/5E65ADD3" Ref="R11"  Part="1" 
F 0 "R11" V 3500 3050 50  0000 C CNN
F 1 "5" V 3600 3050 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 3500 2850 50  0001 C CNN
F 3 "" H 3600 3050 50  0000 C CNN
	1    3600 3050
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5E65E1C9
P 3850 2950
AR Path="/5C2CDB08/5E65E1C9" Ref="#PWR?"  Part="1" 
AR Path="/5E65E1C9" Ref="#PWR0127"  Part="1" 
F 0 "#PWR0127" H 3850 2800 50  0001 C CNN
F 1 "+3V3" H 3865 3123 50  0000 C CNN
F 2 "" H 3850 2950 50  0001 C CNN
F 3 "" H 3850 2950 50  0001 C CNN
	1    3850 2950
	0    -1   -1   0   
$EndComp
Text Label 5450 3100 2    50   ~ 0
CLKB
Wire Wire Line
	5450 3100 5150 3100
Text Label 5450 3200 2    50   ~ 0
CLKA
Wire Wire Line
	5450 3200 5150 3200
Text Label 5450 3300 2    50   ~ 0
D0
Wire Wire Line
	5450 3300 5150 3300
Text Label 5450 3400 2    50   ~ 0
D1
Wire Wire Line
	5450 3400 5150 3400
Text Label 5450 3500 2    50   ~ 0
D2
Wire Wire Line
	5450 3500 5150 3500
Text Label 5450 3600 2    50   ~ 0
D3
Wire Wire Line
	5450 3600 5150 3600
Text Label 5450 3700 2    50   ~ 0
D4
Wire Wire Line
	5450 3700 5150 3700
Text Label 5450 3800 2    50   ~ 0
D5
Wire Wire Line
	5450 3800 5150 3800
Text Label 5450 3900 2    50   ~ 0
D6
Wire Wire Line
	5450 3900 5150 3900
Text Label 5450 4000 2    50   ~ 0
D7
Wire Wire Line
	5450 4000 5150 4000
Text Label 5450 4100 2    50   ~ 0
D8
Wire Wire Line
	5450 4100 5150 4100
Text Label 5450 4200 2    50   ~ 0
D9
Wire Wire Line
	5450 4200 5150 4200
Text Label 5450 4300 2    50   ~ 0
D10
Wire Wire Line
	5450 4300 5150 4300
Text Label 5450 4400 2    50   ~ 0
D11
Wire Wire Line
	5450 4400 5150 4400
Text Label 5450 4500 2    50   ~ 0
D12
Wire Wire Line
	5450 4500 5150 4500
Text Label 5450 4600 2    50   ~ 0
D13
Wire Wire Line
	5450 4600 5150 4600
Text Label 5450 4700 2    50   ~ 0
D14
Wire Wire Line
	5450 4700 5150 4700
Text Label 5450 4800 2    50   ~ 0
D15
Wire Wire Line
	5450 4800 5150 4800
Text Label 5450 4900 2    50   ~ 0
OF
Wire Wire Line
	5450 4900 5150 4900
Text Label 6000 4250 0    50   ~ 0
CLKA
Wire Wire Line
	6000 4250 6300 4250
Text Label 6000 4950 0    50   ~ 0
D0
Wire Wire Line
	6000 4950 6300 4950
Text Label 6000 4850 0    50   ~ 0
D1
Wire Wire Line
	6000 4850 6300 4850
Text Label 6000 4750 0    50   ~ 0
D2
Wire Wire Line
	6000 4750 6300 4750
Text Label 6000 4650 0    50   ~ 0
D3
Wire Wire Line
	6000 4650 6300 4650
Text Label 6000 4550 0    50   ~ 0
D4
Wire Wire Line
	6000 4550 6300 4550
Text Label 6000 4450 0    50   ~ 0
D5
Wire Wire Line
	6000 4450 6300 4450
Text Label 6000 4350 0    50   ~ 0
D6
Wire Wire Line
	6000 4350 6300 4350
Text Label 6000 4150 0    50   ~ 0
D7
Wire Wire Line
	6000 4150 6300 4150
Text Label 6000 4050 0    50   ~ 0
D8
Wire Wire Line
	6000 4050 6300 4050
Text Label 6000 3950 0    50   ~ 0
D9
Wire Wire Line
	6000 3950 6300 3950
Text Label 6000 3850 0    50   ~ 0
D10
Wire Wire Line
	6000 3850 6300 3850
Text Label 6000 3750 0    50   ~ 0
D11
Wire Wire Line
	6000 3750 6300 3750
Text Label 6000 3650 0    50   ~ 0
D12
Wire Wire Line
	6000 3650 6300 3650
Text Label 6000 3550 0    50   ~ 0
D13
Wire Wire Line
	6000 3550 6300 3550
Text Label 6000 3450 0    50   ~ 0
D14
Wire Wire Line
	6000 3450 6300 3450
Text Label 6000 3350 0    50   ~ 0
D15
Wire Wire Line
	6000 3350 6300 3350
Text Label 6000 3250 0    50   ~ 0
OF
Wire Wire Line
	6000 3250 6300 3250
$Comp
L misc:testpoint_1mm TP?
U 1 1 5E851703
P 5550 3100
AR Path="/5BE20C19/5E851703" Ref="TP?"  Part="1" 
AR Path="/5E851703" Ref="TP1"  Part="1" 
F 0 "TP1" H 5700 2900 50  0000 L CNN
F 1 "testpoint_1mm" H 5750 2900 50  0001 L CNN
F 2 "kicad_pcb:TP_0.5mm" H 5750 3000 50  0001 L CNN
F 3 "" V 5800 3100 50  0001 C CNN
	1    5550 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 5450 8200 5500
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E87301C
P 7800 5700
AR Path="/5C2CDB08/5E87301C" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E87301C" Ref="C?"  Part="1" 
AR Path="/5E87301C" Ref="C6"  Part="1" 
F 0 "C6" V 7650 5700 50  0000 C CNN
F 1 "1 uF" V 7950 5700 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 7950 5600 50  0001 L CNN
F 3 "" H 7800 5700 50  0000 C CNN
	1    7800 5700
	1    0    0    -1  
$EndComp
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E873026
P 8200 5700
AR Path="/5C2CDB08/5E873026" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E873026" Ref="C?"  Part="1" 
AR Path="/5E873026" Ref="C14"  Part="1" 
F 0 "C14" V 8050 5700 50  0000 C CNN
F 1 "1 uF" V 8350 5700 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 8350 5600 50  0001 L CNN
F 3 "" H 8200 5700 50  0000 C CNN
	1    8200 5700
	1    0    0    -1  
$EndComp
Connection ~ 8200 5500
Connection ~ 7800 5500
Wire Wire Line
	7800 5500 8200 5500
Wire Wire Line
	7800 5900 8200 5900
Wire Wire Line
	8200 5950 8200 5900
Connection ~ 8200 5900
$Comp
L combined:ground #PWR?
U 1 1 5E8A8EA7
P 8200 5950
AR Path="/5C2CDB08/5E8A8EA7" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E8A8EA7" Ref="#PWR?"  Part="1" 
AR Path="/5E8A8EA7" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 8200 5950 50  0001 C CNN
F 1 "ground" H 8200 5880 50  0001 C CNN
F 2 "" H 8200 5950 50  0001 C CNN
F 3 "" H 8200 5950 50  0000 C CNN
	1    8200 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 5500 7650 5500
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5E8EA35A
P 7650 5500
AR Path="/5C2CDB08/5E8EA35A" Ref="#FLG?"  Part="1" 
AR Path="/5E374CB3/5E8EA35A" Ref="#FLG?"  Part="1" 
AR Path="/5E8EA35A" Ref="#FLG0101"  Part="1" 
F 0 "#FLG0101" H 7650 5575 50  0001 C CNN
F 1 "PWR_FLAG" V 7650 5800 50  0000 C CNN
F 2 "" H 7650 5500 50  0001 C CNN
F 3 "~" H 7650 5500 50  0001 C CNN
	1    7650 5500
	1    0    0    -1  
$EndComp
Connection ~ 7650 5500
Wire Wire Line
	7650 5500 7800 5500
Wire Wire Line
	7300 5500 7150 5500
Connection ~ 7150 5500
Text Label 6600 5500 0    50   ~ 0
1V8I
NoConn ~ 5150 3100
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5E93B301
P 7850 2600
AR Path="/5C2CDB08/5E93B301" Ref="#FLG?"  Part="1" 
AR Path="/5E374CB3/5E93B301" Ref="#FLG?"  Part="1" 
AR Path="/5E93B301" Ref="#FLG0103"  Part="1" 
F 0 "#FLG0103" H 7850 2675 50  0001 C CNN
F 1 "PWR_FLAG" V 7850 2900 50  0000 C CNN
F 2 "" H 7850 2600 50  0001 C CNN
F 3 "~" H 7850 2600 50  0001 C CNN
	1    7850 2600
	1    0    0    -1  
$EndComp
$Comp
L capacitors:capacitor_0603 C15
U 1 1 5E998044
P 7750 2800
F 0 "C15" H 7885 2846 50  0000 L CNN
F 1 "2.2 uF" H 7885 2755 50  0000 L CNN
F 2 "kicad_pcb:C_0603_1608Metric" H 7900 2700 50  0001 L CNN
F 3 "" H 7750 2800 50  0000 C CNN
	1    7750 2800
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E998B4A
P 7750 3000
AR Path="/5C2CDB08/5E998B4A" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E998B4A" Ref="#PWR?"  Part="1" 
AR Path="/5E998B4A" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 7750 3000 50  0001 C CNN
F 1 "ground" H 7750 2930 50  0001 C CNN
F 2 "" H 7750 3000 50  0001 C CNN
F 3 "" H 7750 3000 50  0000 C CNN
	1    7750 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2600 7850 2600
Wire Wire Line
	6200 2600 6950 2600
Connection ~ 6950 2600
Connection ~ 7750 2600
Wire Wire Line
	7750 2600 7700 2600
Connection ~ 7850 2600
Wire Wire Line
	7850 2600 7750 2600
Text Label 6600 2600 0    50   ~ 0
3V8I
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E9E0527
P 9200 3750
AR Path="/5C2CDB08/5E9E0527" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E9E0527" Ref="C?"  Part="1" 
AR Path="/5E9E0527" Ref="C16"  Part="1" 
F 0 "C16" V 9050 3750 50  0000 C CNN
F 1 "1 uF" V 9350 3750 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 9350 3650 50  0001 L CNN
F 3 "" H 9200 3750 50  0000 C CNN
	1    9200 3750
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5E9E099C
P 9200 3550
AR Path="/5C2CDB08/5E9E099C" Ref="#PWR?"  Part="1" 
AR Path="/5E9E099C" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 9200 3400 50  0001 C CNN
F 1 "+3V3" H 9215 3723 50  0000 C CNN
F 2 "" H 9200 3550 50  0001 C CNN
F 3 "" H 9200 3550 50  0001 C CNN
	1    9200 3550
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E9E0EB3
P 9200 3950
AR Path="/5C2CDB08/5E9E0EB3" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E9E0EB3" Ref="#PWR?"  Part="1" 
AR Path="/5E9E0EB3" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 9200 3950 50  0001 C CNN
F 1 "ground" H 9200 3880 50  0001 C CNN
F 2 "" H 9200 3950 50  0001 C CNN
F 3 "" H 9200 3950 50  0000 C CNN
	1    9200 3950
	1    0    0    -1  
$EndComp
NoConn ~ 6300 3050
NoConn ~ 6300 3150
Wire Wire Line
	2250 5400 2200 5400
Connection ~ 2250 5400
Wire Wire Line
	2800 4900 2900 4900
Wire Wire Line
	2800 5900 2900 5900
Wire Wire Line
	3350 5250 3350 5150
Connection ~ 3350 5150
Wire Wire Line
	3350 5150 3250 5150
Wire Wire Line
	3350 5550 3350 5650
Connection ~ 3350 5650
Wire Wire Line
	3350 5650 3250 5650
$Comp
L Device:L L1
U 1 1 5EC1ADC1
P 3350 5400
F 0 "L1" H 3402 5446 50  0000 L CNN
F 1 "47 nH" H 3402 5355 50  0000 L CNN
F 2 "kicad_pcb:L_0402_1005Metric" H 3350 5400 50  0001 C CNN
F 3 "~" H 3350 5400 50  0001 C CNN
	1    3350 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 4500 3850 4600
Connection ~ 3850 4500
$Comp
L misc:6H67G Fl1
U 1 1 5EC6399D
P 7350 2600
F 0 "Fl1" H 7350 2723 50  0000 C CNN
F 1 "6H67G" H 7150 2700 50  0001 L BNN
F 2 "kicad_pcb:NFE31" H 7350 2600 50  0001 L BNN
F 3 "$PARTS/6H67G/NFE31PT222Z1E9#.pdf" H 7350 2600 50  0001 L BNN
	1    7350 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2600 6950 2600
$Comp
L combined:ground #PWR?
U 1 1 5EC9F231
P 7350 2800
AR Path="/5C2CDB08/5EC9F231" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5EC9F231" Ref="#PWR?"  Part="1" 
AR Path="/5EC9F231" Ref="#PWR0128"  Part="1" 
F 0 "#PWR0128" H 7350 2800 50  0001 C CNN
F 1 "ground" H 7350 2730 50  0001 C CNN
F 2 "" H 7350 2800 50  0001 C CNN
F 3 "" H 7350 2800 50  0000 C CNN
	1    7350 2800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
